# math-learning

Repository to historically learn about step-by-step mathematics.

<ins>**Map of Mathematics**</ins>
+ [video] The Map of Mathematics - by [**DoS - Domain of Science**](https://youtu.be/OmJ-4B-mS-Y)

<p align="center">
<img src="/images/math.jpg" width="80%">
</p>

<p align="center">
<img src="/images/math-cheatsheet.jpg" width="80%">
</p>

<ins>**Linear Algebra**</ins>
+ [video-playlist] Essence of linear algebra - by [**3Blue1Brown**](https://youtube.com/playlist?list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab)

<ins>**Optimization**</ins>
+ Bla-Bla

<ins>**Dynamic Systems**</ins>
+ Bla-Bla

<ins>**Probability**</ins>
+ Bla-Bla

<ins>**Neural Network**</ins>
+ [video-playlist] Neural network - by [**3Blue1Brown**](https://youtube.com/playlist?list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi)
